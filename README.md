# Delivery_Service
Задания: 
Создать базовый класс Transport c 3 атрибутами(max_weight, speed и available) и методом delivery_time который в аргументах получает расстояние доставки и возвращает время доставки.

Создать класс Bike который наследует класс Transport. Каждый велосипед может двигаться со скоростью 10 км/ч и может везти посылки до 10 кг. Также у велосипеда есть уникальное свойство max_distance равное 30 км.
  
Создать класс Car который наследует класс Transport. Каждая машина может двигаться со скоростью 50 км/ч и может везти посылки до 100 кг. Также у машины есть уникальное свойство регистрационный номер.

Создать класс DeliveryService который управляет доставками. Парк службы доставки имеет несколько велосипедов и машин(количество определяете сами). Создать 2 метода для этого класса. Первый по параметрам доставки(вес и дистанция) находит подходящий транспорт для доставки. Второй метод подтверждает доставку. 
